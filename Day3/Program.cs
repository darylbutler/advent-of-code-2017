﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3
{
    class Program
    {

        /******
         * --- Day 3: Spiral Memory ---
         * 
         * You come across an experimental new kind of memory stored on an infinite two-dimensional grid.
         * 
         * Each square on the grid is allocated in a spiral pattern starting at a location marked 1 and then counting up while spiraling outward. For example, the first few squares are allocated like this:
         * 
         * 17  16  15  14  13
         * 18   5   4   3  12
         * 19   6   1   2  11
         * 20   7   8   9  10
         * 21  22  23---> ...
         * 
         * While this is very space-efficient (no squares are skipped), requested data must be carried back to square 1 (the location of the only access port for this memory system) by programs that can only move up, down, left, or right. They always take the shortest path: the Manhattan Distance between the location of the data and square 1.
         * 
         * For example:
         * 
         *     Data from square 1 is carried 0 steps, since it's at the access port.
         *     Data from square 12 is carried 3 steps, such as: down, left, left.
         *     Data from square 23 is carried only 2 steps: up twice.
         *     Data from square 1024 must be carried 31 steps.
         * 
         * How many steps are required to carry the data from the square identified in your puzzle input all the way to the access port?
         * 
         * Your puzzle input is 289326.
         * 
         * 
         * Notes:
         * 
         * 37  36  35  34  33  32  31    
         * 38  17  16  15  14  13  30
         * 39  18   5   4   3  12  29
         * 40  19   6   1   2  11  28
         * 41  20   7   8   9  10  27
         * 42  21  22  23  24  25  26
         * 43  44  45  46  47  48  49  50
         * 
         * */
        static void Main(string[] args) {
            const int spiralSize = 1001;
            var s = MakeSpiral(spiralSize);
            //PrintSpiral(s);
            //FindInSpiral(s, 12, out int x, out int y);
            //Console.WriteLine("Found 12 at ({0}, {1}).", x, y);

            var input = 289326;

            // Part 1 - Distance
            FindInSpiral(s, input, out int x, out int y);
            Console.WriteLine("Found {0} at ({1}, {2}).", input, x, y);
            var dist = Math.Abs(x - (spiralSize / 2)) + Math.Abs(y - (spiralSize / 2));
            Console.WriteLine("Distance is {0}.", dist);

            MakeSpiral2(spiralSize, input);

            Console.ReadLine();
        }



        static int[,] MakeSpiral(int width) {
            if (width % 2 == 0)
                throw new ArgumentException("Width must be odd!");

            var spiral = new int[width, width];

            int x = width / 2;
            int y = width / 2;

            var iter = 1;
            var direction = 0;

            Action Set = () => { spiral[x, y] = iter++; };
            Action Move = () => {               
                switch(direction) {
                    case 0:  // Right
                        x++;
                        break;
                    case 1: // Up
                        y--;
                        break;
                    case 2: // Left
                        x--;
                        break;
                    case 3: // Down
                        y++;
                        break;
                }
            };
            Action<int> SetAndMove = (num) => {
                for (int i = 0; i < num; i++) {
                    Move();
                    if (x >= width)
                        return;
                    Set();                  
                }
                // Change direction
                direction = (++direction) % 4;
            };
            // Set origin
            Set();

            var n = 1;
            while(true) {
                SetAndMove(n);
                SetAndMove(n);
                n++;
                if (x >= width)
                    return spiral;
            }
        }
        static void PrintSpiral(int[,] spiral) {
            const int padding = 5;

            for (int r = 0; r <= spiral.GetUpperBound(0); r++) {                
                for (int c = 0; c <= spiral.GetUpperBound(0); c++) {
                    Console.Write(string.Format(" [{0}]", spiral[c, r]).PadLeft(padding));
                }
                Console.WriteLine();
            }
        }
        static void FindInSpiral(int[,] spiral, int toFind, out int x, out int y) {            
            for (int r = 0; r <= spiral.GetUpperBound(0); r++) {
                for (int c = 0; c <= spiral.GetUpperBound(0); c++) {
                    if (spiral[c, r] == toFind) {
                        x = c;
                        y = r;
                        return;
                    }   
                }
            }
            x = -1;
            y = -1;
        }

        static int MakeSpiral2(int width, int input) {
            if (width % 2 == 0)
                throw new ArgumentException("Width must be odd!");

            var spiral = new int[width, width];

            int x = width / 2;
            int y = width / 2;            
            var direction = 0;
            var requestReturn = false;

            Action Set = () => {
                var sum = 0;

                sum += spiral[x - 1, y];
                sum += spiral[x - 1, y - 1];
                sum += spiral[x, y - 1];
                sum += spiral[x + 1, y - 1];
                sum += spiral[x + 1, y];
                sum += spiral[x + 1, y + 1];
                sum += spiral[x, y + 1];
                sum += spiral[x - 1, y + 1];

                if (sum > input) {
                    Console.WriteLine("Sum Larger than Input: {0}", sum);
                    requestReturn = true;
                }

                spiral[x, y] = sum > 0 ? sum : 1;
            };
            Action Move = () => {
                switch (direction) {
                    case 0:  // Right
                        x++;
                        break;
                    case 1: // Up
                        y--;
                        break;
                    case 2: // Left
                        x--;
                        break;
                    case 3: // Down
                        y++;
                        break;
                }
            };
            Action<int> SetAndMove = (num) => {
                for (int i = 0; i < num; i++) {
                    Move();
                    if (x >= width)
                        return;
                    Set();
                }
                // Change direction
                direction = (++direction) % 4;
            };
            // Set origin
            Set();

            var n = 1;
            while (true) {
                SetAndMove(n);
                SetAndMove(n);
                n++;

                if (requestReturn)
                    return 1;
            }
        }
    }
}
