﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13
{
    class Program
    {
        /*--- Day 13: Packet Scanners ---
         * 
         * You need to cross a vast firewall. The firewall consists of several layers, each with a security scanner that moves back and forth across the layer. To succeed, you must not be detected by a scanner.
         * 
         * By studying the firewall briefly, you are able to record (in your puzzle input) the depth of each layer and the range of the scanning area for the scanner within it, written as depth: range. Each layer has a thickness of exactly 1. A layer at depth 0 begins immediately inside the firewall; a layer at depth 1 would start immediately after that.
         * 
         * For example, suppose you've recorded the following:
         * 
         * 0: 3
         * 1: 2
         * 4: 4
         * 6: 4
         * 
         * This means that there is a layer immediately inside the firewall (with range 3), a second layer immediately after that (with range 2), a third layer which begins at depth 4 (with range 4), and a fourth layer which begins at depth 6 (also with range 4). Visually, it might look like this:
         * 
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... ... [ ] ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * Within each layer, a security scanner moves back and forth within its range. Each security scanner starts at the top and moves down until it reaches the bottom, then moves up until it reaches the top, and repeats. A security scanner takes one picosecond to move one step. Drawing scanners as S, the first few picoseconds look like this:
         * 
         * 
         * Picosecond 0:
         *  0   1   2   3   4   5   6
         * [S] [S] ... ... [S] ... [S]
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * Picosecond 1:
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... ... [ ] ... [ ]
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * Picosecond 2:
         *  0   1   2   3   4   5   6
         * [ ] [S] ... ... [ ] ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [S]             [S]     [S]
         *                 [ ]     [ ]
         * 
         * Picosecond 3:
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... ... [ ] ... [ ]
         * [S] [S]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [S]     [S]
         * 
         * Your plan is to hitch a ride on a packet about to move through the firewall. The packet will travel along the top of each layer, and it moves at one layer per picosecond. Each picosecond, the packet moves one layer forward (its first move takes it into layer 0), and then the scanners move one step. If there is a scanner at the top of the layer as your packet enters it, you are caught. (If a scanner moves into the top of its layer while you are there, you are not caught: it doesn't have time to notice you before you leave.) If you were to do this in the configuration above, marking your current position with parentheses, your passage through the firewall would look like this:
         * 
         * Initial state:
         *  0   1   2   3   4   5   6
         * [S] [S] ... ... [S] ... [S]
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * Picosecond 0:
         *  0   1   2   3   4   5   6
         * (S) [S] ... ... [S] ... [S]
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * ( ) [ ] ... ... [ ] ... [ ]
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 1:
         *  0   1   2   3   4   5   6
         * [ ] ( ) ... ... [ ] ... [ ]
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [ ] (S) ... ... [ ] ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [S]             [S]     [S]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 2:
         *  0   1   2   3   4   5   6
         * [ ] [S] (.) ... [ ] ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [S]             [S]     [S]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [ ] [ ] (.) ... [ ] ... [ ]
         * [S] [S]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [S]     [S]
         * 
         * 
         * Picosecond 3:
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... (.) [ ] ... [ ]
         * [S] [S]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [S]     [S]
         * 
         *  0   1   2   3   4   5   6
         * [S] [S] ... (.) [ ] ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [S]     [S]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 4:
         *  0   1   2   3   4   5   6
         * [S] [S] ... ... ( ) ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [S]     [S]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... ... ( ) ... [ ]
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 5:
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... ... [ ] (.) [ ]
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [ ] [S] ... ... [S] (.) [S]
         * [ ] [ ]         [ ]     [ ]
         * [S]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 6:
         *  0   1   2   3   4   5   6
         * [ ] [S] ... ... [S] ... (S)
         * [ ] [ ]         [ ]     [ ]
         * [S]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... ... [ ] ... ( )
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * In this situation, you are caught in layers 0 and 6, because your packet entered the layer when its scanner was at the top when you entered it. You are not caught in layer 1, since the scanner moved into the top of the layer once you were already there.
         * 
         * The severity of getting caught on a layer is equal to its depth multiplied by its range. (Ignore layers in which you do not get caught.) The severity of the whole trip is the sum of these values. In the example above, the trip severity is 0*3 + 6*4 = 24.
         * 
         * Given the details of the firewall you've recorded, if you leave immediately, what is the severity of your whole trip?
         * 
         * --- Part Two ---
         * 
         * Now, you need to pass through the firewall without being caught - easier said than done.
         * 
         * You can't control the speed of the packet, but you can delay it any number of picoseconds. For each picosecond you delay the packet before beginning your trip, all security scanners move one step. You're not in the firewall during this time; you don't enter layer 0 until you stop delaying the packet.
         * 
         * In the example above, if you delay 10 picoseconds (picoseconds 0 - 9), you won't get caught:
         * 
         * State after delaying:
         *  0   1   2   3   4   5   6
         * [ ] [S] ... ... [ ] ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [S]             [S]     [S]
         *                 [ ]     [ ]
         * 
         * Picosecond 10:
         *  0   1   2   3   4   5   6
         * ( ) [S] ... ... [ ] ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [S]             [S]     [S]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * ( ) [ ] ... ... [ ] ... [ ]
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 11:
         *  0   1   2   3   4   5   6
         * [ ] ( ) ... ... [ ] ... [ ]
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [S] (S) ... ... [S] ... [S]
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 12:
         *  0   1   2   3   4   5   6
         * [S] [S] (.) ... [S] ... [S]
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [ ] [ ] (.) ... [ ] ... [ ]
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 13:
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... (.) [ ] ... [ ]
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [ ] [S] ... (.) [ ] ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [S]             [S]     [S]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 14:
         *  0   1   2   3   4   5   6
         * [ ] [S] ... ... ( ) ... [ ]
         * [ ] [ ]         [ ]     [ ]
         * [S]             [S]     [S]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... ... ( ) ... [ ]
         * [S] [S]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [S]     [S]
         * 
         * 
         * Picosecond 15:
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... ... [ ] (.) [ ]
         * [S] [S]         [ ]     [ ]
         * [ ]             [ ]     [ ]
         *                 [S]     [S]
         * 
         *  0   1   2   3   4   5   6
         * [S] [S] ... ... [ ] (.) [ ]
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [S]     [S]
         *                 [ ]     [ ]
         * 
         * 
         * Picosecond 16:
         *  0   1   2   3   4   5   6
         * [S] [S] ... ... [ ] ... ( )
         * [ ] [ ]         [ ]     [ ]
         * [ ]             [S]     [S]
         *                 [ ]     [ ]
         * 
         *  0   1   2   3   4   5   6
         * [ ] [ ] ... ... [ ] ... ( )
         * [S] [S]         [S]     [S]
         * [ ]             [ ]     [ ]
         *                 [ ]     [ ]
         * 
         * Because all smaller delays would get you caught, the fewest number of picoseconds you would need to delay to get through safely is 10.
         * 
         * What is the fewest number of picoseconds that you need to delay the packet to pass through the firewall without being caught?
         * 
         * 
         * 
         **/
        static void Main(string[] args) {
            var test = new List<string> {
                "0: 3",
                "1: 2",
                "4: 4",
                "6: 4",
            };
            var input = System.IO.File.ReadAllLines("input").ToList();

            Console.WriteLine("--- Part 1: Measure Severity with 0 delay");
            Console.WriteLine("Test: {0}", MeasureSeverity(test));
            Console.WriteLine("Input: {0}", MeasureSeverity(input));

            Console.WriteLine("--- Part 2: Find Shortest Delay to not get caught");
            //for (var delay = 0; ; delay++) {
            //    var severity = MeasureSeverity(input, delay);

            //    if (severity == 0) {
            //        Console.WriteLine("Needed Delay: {0}", delay);
            //        break;
            //    }
            //}
            var t = FindHowNotToGetCaught(input);
            Console.WriteLine("Test: {0}", t);
            Console.ReadLine();
        }


        static int MeasureSeverity(List<string> input, int delay = 0) {
            var severity = 0;
            var iter = -1;   // How many passes we made
            var scanners = new Dictionary<int, int>(); // (Layer, Range)

            // -- Parse
            foreach (var s in input) {
                var split = s.Split(':');
                if (split.Count() == 2) {
                    scanners[int.Parse(split[0])] = int.Parse(split[1]);
                }
            }

            // Build the firewall
            var depth = scanners.Keys.Max();
            var scan_pos = new Dictionary<int, int>();  // (Layer, pos in range)
            var scan_dir = new Dictionary<int, int>();  // (Layer, direction(0 = inc, 1 = dec))
            var my_pos = -1; // First move brings us into 0
            foreach (var l in scanners.Keys) { scan_pos[l] = 0; scan_dir[l] = 0; }   // Scanners start at pos 0

            while (my_pos < depth) {
                iter++;

                // Advance if delay is due
                if (iter >= delay) {
                    my_pos++;
                }

                // If we move in while there's a scanner at pos 0, we're caught
                if (my_pos >= 0 && scan_pos.ContainsKey(my_pos) && scan_pos[my_pos] == 0) {
                    severity += (my_pos * scanners[my_pos]);
                }

                // Move the scanners
                foreach (var scanner in scanners) {
                    var layer = scanner.Key;
                    var range = scanner.Value;

                    if (scan_dir[layer] == 0 && scan_pos[layer] >= range - 1) {
                        scan_dir[layer] = 1;
                    } else if (scan_dir[layer] == 1 && scan_pos[layer] == 0) {
                        scan_dir[layer] = 0;
                    }

                    if (scan_dir[layer] == 0)
                        scan_pos[layer]++;
                    else
                        scan_pos[layer]--;

                }
            }

            return severity;
        }
        static int FindHowNotToGetCaught(List<string> input) {
            var iter = -1;   // How many passes we made
            var scanners = new Dictionary<int, int>(); // (Layer, Range)

            // -- Parse
            foreach (var s in input) {
                var split = s.Split(':');
                if (split.Count() == 2) {
                    scanners[int.Parse(split[0])] = int.Parse(split[1]);
                }
            }

            // Build the firewall
            var depth = scanners.Keys.Max();
            var scan_pos = new Dictionary<int, int>();  // (Layer, pos in range)
            var scan_dir = new Dictionary<int, int>();  // (Layer, direction(0 = inc, 1 = dec))
            var results = new Dictionary<int, bool>();  // (delay, passed)
            foreach (var l in scanners.Keys) { scan_pos[l] = 0; scan_dir[l] = 0; }   // Scanners start at pos 0

            while (true) {
                iter++;

                // Add the new iter
                results[iter] = (scan_pos[0] != 0);    

                // Check each relevate delay if it's caugh this round
                for (var i = 1; i <= depth; i++) {
                    var pos = iter - i;
                    if (pos < 0) break;

                    if (scan_pos.ContainsKey(i) && scan_pos[i] == 0)
                        results[pos] = false;

                    if (i == depth && results[pos])
                        return pos;
                }

                // Move the scanners
                foreach (var scanner in scanners) {
                    var layer = scanner.Key;
                    var range = scanner.Value;

                    if (scan_dir[layer] == 0 && scan_pos[layer] >= range - 1) {
                        scan_dir[layer] = 1;
                    }
                    else if (scan_dir[layer] == 1 && scan_pos[layer] == 0) {
                        scan_dir[layer] = 0;
                    }

                    if (scan_dir[layer] == 0)
                        scan_pos[layer]++;
                    else
                        scan_pos[layer]--;

                }
            }
        }
    }
}
