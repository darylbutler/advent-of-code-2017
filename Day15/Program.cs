﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day15
{
    class Program
    {
        static void Main(string[] args) {
            var genA = new Generator(618, 16807, 4);
            var genB = new Generator(814, 48271, 8);

            var testA = new Generator(65, 16807, 4);
            var testB = new Generator(8921, 48271, 8);


            //for (int i = 0; i < 5; i++) {
            //    Console.WriteLine("{0} -- GenA: {1}, GenB: {2}", i, testA.NextGoodValue(), testB.NextGoodValue());
            //}

            // -- Speed Test
            //var a = new System.Diagnostics.Stopwatch();
            //a.Start();
            //for (var i = 0; i < 100000000; i++) {
            //    var u = Lowest16Bits_2(0b00001110101000101110001101001010);
            //}
            //a.Stop();
            //Console.WriteLine("Part 1: {0}ms", a.ElapsedMilliseconds);
            //a.Restart();
            //for (var i = 0; i < 100000000; i++) {
            //    var u = Lowest16Bits_1(0b00001110101000101110001101001010);
            //}
            //a.Stop();
            //Console.WriteLine("Part 2: {0}ms", a.ElapsedMilliseconds);
            //Console.WriteLine("Test 1: {0}", Test_Part1(testA, testB));
            //Console.WriteLine("Test 1: {0}", Test_Part1(genA, genB));

            //Console.WriteLine("Test 2: {0}", Test_Part2(testA, testB));
            Console.WriteLine("Test 2: {0}", Test_Part2(genA, genB));

            Console.ReadLine();
        }

        static int Test_Part1(Generator genA, Generator genB) {
            var score = 0;
            for (int i = 0; i < 40_000_000; i++) {
                var a = genA.NextValue();
                var b = genB.NextValue();

                if (Lowest16Bits_1(a) == Lowest16Bits_1(b))
                    score++;
            }

            return score;
        }
        static int Test_Part2(Generator genA, Generator genB) {
            var score = 0;
            for (int i = 0; i < 5_000_000; i++) {
                var a = genA.NextGoodValue();
                var b = genB.NextGoodValue();

                if (Lowest16Bits_1(a) == Lowest16Bits_1(b))
                    score++;
            }

            return score;
        }

        static int Lowest16Bits_1(long a) {
            return (((int)a) << 16) >> 16;
        }
        static int Lowest16Bits_2(long a) {
            return (short)a;    // Much faster
        }
    }
    

    class Generator
    {
        long PreviousValue, Factor, Multiple;

        public Generator(long startingVal, long factor, long mult) {
            PreviousValue = startingVal;
            Factor = factor;
            Multiple = mult;
        }
        public long NextValue() {
            PreviousValue = (PreviousValue * Factor) % 2147483647;
            return PreviousValue;
        }
        public long NextGoodValue() {
            while (true) {
                var val = NextValue();
                if (val % Multiple == 0)
                    return val;
            }
        }
    }
}
