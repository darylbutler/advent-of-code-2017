﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7
{
    /*
     * 
     * --- Day 7: Recursive Circus ---
     * 
     * Wandering further through the circuits of the computer, you come upon a tower of programs that have gotten themselves into a bit of trouble. A recursive algorithm has gotten out of hand, and now they're balanced precariously in a large tower.
     * 
     * One program at the bottom supports the entire tower. It's holding a large disc, and on the disc are balanced several more sub-towers. At the bottom of these sub-towers, standing on the bottom disc, are other programs, each holding their own disc, and so on. At the very tops of these sub-sub-sub-...-towers, many programs stand simply keeping the disc below them balanced but with no disc of their own.
     * 
     * You offer to help, but first you need to understand the structure of these towers. You ask each program to yell out their name, their weight, and (if they're holding a disc) the names of the programs immediately above them balancing on that disc. You write this information down (your puzzle input). Unfortunately, in their panic, they don't do this in an orderly fashion; by the time you're done, you're not sure which program gave which information.
     * 
     * For example, if your list is the following:
     * 
     * pbga (66)
     * xhth (57)
     * ebii (61)
     * havc (66)
     * ktlj (57)
     * fwft (72) -> ktlj, cntj, xhth
     * qoyq (66)
     * padx (45) -> pbga, havc, qoyq
     * tknk (41) -> ugml, padx, fwft
     * jptl (61)
     * ugml (68) -> gyxo, ebii, jptl
     * gyxo (61)
     * cntj (57)
     * 
     * ...then you would be able to recreate the structure of the towers that looks like this:
     * 
     *                 gyxo
     *               /     
     *          ugml - ebii
     *        /      \     
     *       |         jptl
     *       |        
     *       |         pbga
     *      /        /
     * tknk --- padx - havc
     *      \        \
     *       |         qoyq
     *       |             
     *       |         ktlj
     *        \      /     
     *          fwft - cntj
     *               \     
     *                 xhth
     * 
     * In this example, tknk is at the bottom of the tower (the bottom program), and is holding up ugml, padx, and fwft. Those programs are, in turn, holding up other programs; in this example, none of those programs are holding up any other programs, and are all the tops of their own towers. (The actual tower balancing in front of you is much larger.)
     * 
     * Before you're ready to help them, you need to make sure your information is correct. What is the name of the bottom program?
     * 
     * 
     * 
     * */

    class Program
    {
        static string badNodeName = string.Empty;
        static int badNodeOffset = 0;

        static void Main(string[] args) {
            var input = System.IO.File.ReadAllLines("input.txt").ToList();

            var test = new List<string> {
                "pbga (66)",
                "xhth (57)",
                "ebii (61)",
                "havc (66)",
                "ktlj (57)",
                "fwft (72) -> ktlj, cntj, xhth",
                "qoyq (66)",
                "padx (45) -> pbga, havc, qoyq",
                "tknk (41) -> ugml, padx, fwft",
                "jptl (61)",
                "ugml (68) -> gyxo, ebii, jptl",
                "gyxo (61)",
                "cntj (57)",
            };

            // All Phases: Build the tree
            var root = BuildProgramTree(input);

            // Phase 1: Name of Root node
            Console.WriteLine(root.Name);

            // Phase 2: Find bad Node and the weight it takes to balance the tree
            var bad_node = FindMissingWeight(root);
            if (bad_node.Name == root.Name) {
                Console.WriteLine("No bad nodes found!");                
            } else {
                var bad_node_diff = GetBalanceDiff(root);

                Console.WriteLine("Bad Node Found. Name: {0}, Current Weight: {1}, Proposed Weight: {2}",
                    bad_node.Name,
                    bad_node.Weight,
                    bad_node.Weight + bad_node_diff);

                // Test
                badNodeName = bad_node.Name;
                badNodeOffset = bad_node_diff;

                bad_node = FindMissingWeight(root);
                if (bad_node.Name == root.Name) {
                    Console.WriteLine("Recheck Successful!");
                } else {
                    Console.WriteLine("Recheck Failed!");
                }
            }            
            


            Console.ReadLine();
        }

        static prg BuildProgramTree(List<string> input) {
            var programs = new List<prg>(input.Count);
            var prg_to_childs = new Dictionary<string, List<string>>();
            var potential_roots = new List<string>();

            // First, Parse in input list to build a list of children and capture the names of their children
            foreach (var line in input) {
                var tokens = line.Split(' ');
                var p = new prg() {
                    Name = tokens[0],
                    Weight = int.Parse(tokens[1].Substring(1, tokens[1].Length - 2))
                };
                programs.Add(p);

                if (tokens.Count() > 2 && tokens[2] == "->") { // Has children
                    prg_to_childs[p.Name] = line.Substring(line.IndexOf("->") + 3).Split(',').Select(x => x.Trim()).ToList();
                    potential_roots.Add(p.Name);
                }
            }

            // Now, assign the children
            foreach (var p in prg_to_childs) {
                var parent = programs.Find(x => x.Name == p.Key);
                parent.Children = new List<prg>();

                foreach (var c in p.Value) {
                    parent.Children.Add(programs.Find(x => x.Name == c));
                    if (potential_roots.Contains(c))
                        potential_roots.Remove(c);
                }
            }

            // Find the root node (The root node owns sometihng, but isn't owned by anyone else)
            if (potential_roots.Count > 1 || potential_roots.Count == 0)
                throw new Exception("No Possible Root Nodes!");
            return programs.Find(x => x.Name == potential_roots[0]);
        }
        static int GetNodesWeight(prg Node) {
            var sum = Node.Weight;

            // Test, increase the bad node's weight to see if the tree is balanced
            if (badNodeName != string.Empty && Node.Name == badNodeName)
                sum += badNodeOffset;   // test

            if (Node.Children?.Count > 0) {
                foreach (var c in Node.Children) {
                    sum += GetNodesWeight(c);
                }
            }
                
            return sum;
        }
        static prg FindMissingWeight(prg Node) {
            var sums = new List<int>(Node.Children.Count);

            // -- Sum each branch
            foreach (var c in Node.Children) {
                var sum = GetNodesWeight(c);

                sums.Add(sum);
            }

            // -- Find the correct sum
            var correct_sum = 0;
            if (sums[0] == sums[1] || sums[0] == sums[2])
                correct_sum = sums[0];
            else
                correct_sum = sums[1];

            // Find the node with the incorrect sum
            var incorrect_node = sums.FindIndex(x => x != correct_sum);            

            // The bad node will have all nodes equal weight
            if (incorrect_node < 0) {
                // We've found the bad node, this node's weight must chnage
                return Node;
            }
            else {
                // Our nodes are still bad, keep going down the chain
                return FindMissingWeight(Node.Children[incorrect_node]);
            }
        }

        static int GetBalanceDiff(prg Node) {
            var sums = new List<int>(Node.Children.Count);

            // -- Sum each branch
            foreach (var c in Node.Children) {
                var sum = GetNodesWeight(c);

                sums.Add(sum);
            }

            // -- Find the correct sum
            var correct_sum = 0;
            if (sums[0] == sums[1] || sums[0] == sums[2])
                correct_sum = sums[0];
            else
                correct_sum = sums[1];

            // -- Find the node with the incorrect sum
            var incorrect_node = sums.FindIndex(x => x != correct_sum);

            // -- Return Differnce Delta
            if (incorrect_node < 0) return 0;   // Node balanced already

            return correct_sum - sums[incorrect_node];
        }

        class prg
        {
            public int Weight;
            public string Name;
            public List<prg> Children;
        }
    }
}
