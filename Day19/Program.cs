﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day19
{
    class Program
    {
        /*
         * --- Day 19: A Series of Tubes ---
         * 
         * Somehow, a network packet got lost and ended up here. It's trying to follow a routing diagram (your puzzle input), but it's confused about where to go.
         * 
         * Its starting point is just off the top of the diagram. Lines (drawn with |, -, and +) show the path it needs to take, starting by going down onto the only line connected to the top of the diagram. It needs to follow this path until it reaches the end (located somewhere within the diagram) and stop there.
         * 
         * Sometimes, the lines cross over each other; in these cases, it needs to continue going the same direction, and only turn left or right when there's no other option. In addition, someone has left letters on the line; these also don't change its direction, but it can use them to keep track of where it's been. For example:
         * 
         *      |          
         *      |  +--+    
         *      A  |  C    
         *  F---|----E|--+ 
         *      |  |  |  D 
         *      +B-+  +--+ 
         * 
         * Given this diagram, the packet needs to take the following path:
         * 
         *     Starting at the only line touching the top of the diagram, it must go down, pass through A, and continue onward to the first +.
         *     Travel right, up, and right, passing through B in the process.
         *     Continue down (collecting C), right, and up (collecting D).
         *     Finally, go all the way left through E and stopping at F.
         * 
         * Following the path to the end, the letters it sees on its path are ABCDEF.
         * 
         * The little packet looks up at you, hoping you can help it find the way. What letters will it see (in the order it would see them) if it follows the path? (The routing diagram is very wide; make sure you view it without line wrapping.)
         * 
         * 
         * 
         * 
         */

        static void Main(string[] args) {
            var input = System.IO.File.ReadAllLines("input.txt").ToList();
            //var input = new List<string> {
            //     "     |          ",
            //     "     |  +--+    ",
            //     "     A  |  C    ",
            //     " F---|----E|--+ ",
            //     "     |  |  |  D ",
            //     "     +B-+  +--+ ",
            //};

            var path = Part1_GetPath(input);

            // print path
            Console.WriteLine("> Path: {0}", string.Join("", path));

            Console.ReadLine();
        }

        static List<char> Part1_GetPath(List<string> input) {
            if (input.Count < 1) return new List<char>();

            // pos in the input
            var x = -1;
            var y = -1;
            var dir = 2; // 0 = up, 1 = right, 2 = down, 3 = left

            // return path (Letters encountered)
            var path = new List<char>();

            // Find start pos
            for (int i = 0; i < input[0].Length; i++) {
                if (input[0][i] == '|') {
                    x = i;
                    y = 0;
                    break;
                }
            }

            // Return if first line isn't found
            if (x < 0 || y < 0) return path;

            // Function to Get Char (Return a space char if out of bounds)
            Func<int, int, char> GetCharAt = (iX, iY) => {
                if (iX < 0 || iY < 0 || iY >= input.Count || iX >= input[iY].Length) return ' ';
                return input[iY][iX];
            };
            Func<char> GetCharInDirection = () => {
                if (dir == 0)
                    return GetCharAt(x, y - 1);
                if (dir == 1)
                    return GetCharAt(x + 1, y);
                if (dir == 2)
                    return GetCharAt(x, y + 1);
                if (dir == 3)
                    return GetCharAt(x - 1, y);
                throw new Exception("Wrong Direction");
            };

            // Loop until done
            while (true) {
                char c = GetCharAt(x, y);

                // If we hit a Letter, add it to the path
                if (c >= 'A' && c <= 'Z') {
                    path.Add(c);
                }

                // If we hit a blank space, we're done with the path
                if (c == ' ') {
                    return path;
                }

                // If we hit a plus, change direction
                if (c == '+' & GetCharInDirection() == ' ') {
                    // Check which direction
                    if (dir == 0 || dir == 2) {
                        // Check left or right
                        if (GetCharAt(x - 1, y) != ' ') {
                            dir = 3;
                        }
                        else if (GetCharAt(x + 1, y) != ' ') {
                            dir = 1;
                        }
                        else throw new Exception("Coundn't find left or right at plus!");
                    }
                    else if (dir == 1 || dir == 3) {
                        // Check up or down
                        if (GetCharAt(x, y + 1) != ' ') {
                            dir = 2;
                        }
                        else if (GetCharAt(x, y - 1) != ' ') {
                            dir = 0;
                        }
                        else throw new Exception("Coundn't find up or down at plus!");
                    }
                }

                // Move to next coord
                NextCoord(dir, ref x, ref y);
            }
        }
        static void NextCoord(int dir, ref int x, ref int y) {
            if (dir == 0) {
                y--;
            } else if (dir == 1) {
                x++;
            } else if (dir == 2) {
                y++;
            } else if (dir == 3) {
                x--;
            }
        }
    }
}
