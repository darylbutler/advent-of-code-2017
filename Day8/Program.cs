﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day8
{
    /*
     * --- Day 8: I Heard You Like Registers ---
     * 
     * You receive a signal directly from the CPU. Because of your recent assistance with jump instructions, it would like you to compute the result of a series of unusual register instructions.
     * 
     * Each instruction consists of several parts: the register to modify, whether to increase or decrease that register's value, the amount by which to increase or decrease it, and a condition. If the condition fails, skip the instruction without modifying the register. The registers all start at 0. The instructions look like this:
     * 
     * b inc 5 if a > 1
     * a inc 1 if b < 5
     * c dec -10 if a >= 1
     * c inc -20 if c == 10
     * 
     * These instructions would be processed as follows:
     * 
     *     Because a starts at 0, it is not greater than 1, and so b is not modified.
     *     a is increased by 1 (to 1) because b is less than 5 (it is 0).
     *     c is decreased by -10 (to 10) because a is now greater than or equal to 1 (it is 1).
     *     c is increased by -20 (to -10) because c is equal to 10.
     * 
     * After this process, the largest value in any register is 1.
     * 
     * You might also encounter <= (less than or equal to) or != (not equal to). However, the CPU doesn't have the bandwidth to tell you what all the registers are named, and leaves that to you to determine.
     * 
     * What is the largest value in any register after completing the instructions in your puzzle input?
     * 
     * --- Part Two ---
     * 
     * To be safe, the CPU also needs to know the highest value held in any register during this process so that it can decide how much memory to allocate to these operations. For example, in the above instructions, the highest value ever held was 10 (in register c after the third instruction was evaluated).
     * 
     **/
    class Program
    {
        static void Main(string[] args) {
            var input = System.IO.File.ReadAllLines("input.txt").ToList();
            //var input = new List<string> {
            //    "b inc 5 if a > 1",
            //    "a inc 1 if b < 5",
            //    "c dec -10 if a >= 1",
            //    "c inc -20 if c == 10",
            //};

            Console.WriteLine("Part 1:");
            var h = ComputeInstructions(input);
            Console.WriteLine("Largest Register's value: {0}", h.Item1);
            Console.WriteLine("Largest Reg Ever value:   {0}", h.Item2);

            Console.ReadLine();
        }



        static Tuple<int, int> ComputeInstructions(List<string> opCodes) {
            const int REG_DEFAULT_VAL = 0;
            int largest_ever = 0;

            var registers = new Dictionary<string, int>();

            foreach (var op in opCodes) {
                var op_split = op.Split(' ');

                if (op_split.Count() != 7 && op_split[3] != "if")
                    throw new Exception(string.Format("Invalid Op Code: {0}", op));

                // Variable names
                var reg_to_change = op_split[0];
                var val = int.Parse(op_split[2]);
                if (op_split[1] != "inc") val *= -1;    // No need for decrement, we can add a negitive number here
                var reg_to_check = op_split[4];
                var check_op = op_split[5];
                var check_val = int.Parse(op_split[6]);

                // Check if these registers exist. If they don't add them with the default value
                if (!registers.ContainsKey(reg_to_change)) {
                    registers[reg_to_change] = 0;
                }
                if (!registers.ContainsKey(reg_to_check)) {
                    registers[reg_to_check] = 0;
                }

                // Modify the register if the check is true
                if (PerformCheck(check_op, check_val, registers[reg_to_check])) {
                    registers[reg_to_change] += val;

                    if (registers[reg_to_change] > largest_ever) {
                        largest_ever = registers[reg_to_change];
                    }
                }
            }

            // Debug
            if (false) {
                foreach (var reg in registers.Keys)
                    Console.WriteLine("\tRegister \"{0}\": {1}", reg, registers[reg]);
            }

            // Return the largest register
            return new Tuple<int, int>(registers.Values.Max(), largest_ever);
        }

        static bool PerformCheck(string op, int check_val, int reg_val) {
            switch(op) {
                case "==":
                    return reg_val == check_val;
                case "!=":
                    return reg_val != check_val;
                case ">":
                    return reg_val > check_val;
                case ">=":
                    return reg_val >= check_val;
                case "<":
                    return reg_val < check_val;
                case "<=":
                    return reg_val <= check_val;
                default:
                    throw new Exception("Unknown Check Op! OP: " + op);
            }
        }
    }
}
