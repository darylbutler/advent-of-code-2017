﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14
{
    class Program
    {
        /* --- Day 14: Disk Defragmentation ---
         * 
         * Suddenly, a scheduled job activates the system's disk defragmenter. Were the situation different, you might sit and watch it for a while, but today, you just don't have that kind of time. It's soaking up valuable system resources that are needed elsewhere, and so the only option is to help it finish its task as soon as possible.
         * 
         * The disk in question consists of a 128x128 grid; each square of the grid is either free or used. On this disk, the state of the grid is tracked by the bits in a sequence of knot hashes.
         * 
         * A total of 128 knot hashes are calculated, each corresponding to a single row in the grid; each hash contains 128 bits which correspond to individual grid squares. Each bit of a hash indicates whether that square is free (0) or used (1).
         * 
         * The hash inputs are a key string (your puzzle input), a dash, and a number from 0 to 127 corresponding to the row. For example, if your key string were flqrgnkx, then the first row would be given by the bits of the knot hash of flqrgnkx-0, the second row from the bits of the knot hash of flqrgnkx-1, and so on until the last row, flqrgnkx-127.
         * 
         * The output of a knot hash is traditionally represented by 32 hexadecimal digits; each of these digits correspond to 4 bits, for a total of 4 * 32 = 128 bits. To convert to bits, turn each hexadecimal digit to its equivalent binary value, high-bit first: 0 becomes 0000, 1 becomes 0001, e becomes 1110, f becomes 1111, and so on; a hash that begins with a0c2017... in hexadecimal would begin with 10100000110000100000000101110000... in binary.
         * 
         * Continuing this process, the first 8 rows and columns for key flqrgnkx appear as follows, using # to denote used squares, and . to denote free ones:
         * 
         * ##.#.#..-->
         * .#.#.#.#   
         * ....#.#.   
         * #.#.##.#   
         * .##.#...   
         * ##..#..#   
         * .#...#..   
         * ##.#.##.-->
         * |      |   
         * V      V   
         * 
         * In this example, 8108 squares are used across the entire 128x128 grid.
         * 
         * Given your actual key string, how many squares are used?
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         **/
        static void Main(string[] args) {
            Console.WriteLine("Part 1: {0}", CountUsed("wenycdww"));
            Console.WriteLine("Part 2: {0}", CountGroups("wenycdww"));


            Console.ReadLine();
        }

        static bool[,] disk = new bool[128, 128];
        static bool[,] processed = new bool[128, 128];
        static List<List<Point>> groups = new List<List<Point>>();

        static int CountUsed(string input) {
            var used = 0;
            // Build the strings
            var keys = SeedToHashes(input, 128);
            // Build the space            
            for (var i = 0; i < 128; i++) {
                var col = 0;

                foreach (var c in keys[i]) {
                    var bits = HexToBits(c);
                    for (var x = 0; x < 4; x++) {
                        disk[i, col + x] = bits[x];
                        if (bits[x]) used++;
                    }
                    col += 4;
                }
            }
            return used;
        }
        static int CountGroups(string input) {
            var cntUsed = CountUsed(input);
            groups = new List<List<Point>>(cntUsed);

            for (var y = 0; y < 128; y++) {
                for (var x = 0; x < 128; x++) {
                    if (processed[x, y] || !disk[x, y])
                        continue;

                    var grp = groups.Count;
                    groups.Add(new List<Point>());

                    // Add all connected Points
                    GroupByAddress(x, y, grp);
                }
            }

            return groups.Count;
        }
        static void GroupByAddress(int x, int y, int grp) {
            if (x >= 0 && x < 128 && y >= 0 && y < 128 && disk[x, y] && !processed[x, y]) {
                processed[x, y] = true;
                groups[grp].Add(new Point(x, y));

                // Check each related point
                GroupByAddress(x - 1, y, grp);
                GroupByAddress(x + 1, y, grp);
                GroupByAddress(x, y - 1, grp);
                GroupByAddress(x , y + 1, grp);
            }
        }

        class Point
        {
            int x = 0, y = 0;
            public Point(int a, int b) {
                x = a;
                y = b;
            }
        }
        static bool[] HexToBits(char h) {
            switch (h) {
                case '0':
                    return new bool[] { false, false, false, false };
                case '1':
                    return new bool[] { false, false, false, true };
                case '2':
                    return new bool[] { false, false, true, false };
                case '3':
                    return new bool[] { false, false, true, true };
                case '4':
                    return new bool[] { false, true, false, false };
                case '5':
                    return new bool[] { false, true, false, true };
                case '6':
                    return new bool[] { false, true, true, false };
                case '7':
                    return new bool[] { false, true, true, true };
                case '8':
                    return new bool[] { true, false, false, false };
                case '9':
                    return new bool[] { true, false, false, true };
                case 'a':
                    return new bool[] { true, false, true, false };
                case 'b':
                    return new bool[] { true, false, true, true };
                case 'c':
                    return new bool[] { true, true, false, false };
                case 'd':
                    return new bool[] { true, true, false, true };
                case 'e':
                    return new bool[] { true, true, true, false };
                case 'f':
                    return new bool[] { true, true, true, true };
            }

            throw new ArgumentException("Unknown hex character!");
        }
        static List<string> SeedToHashes(string seed, int num) {
            var list = new List<string>(num);
            for (var i = 0; i < num; i++) {
                var n = string.Format("{0}-{1}", seed, i);
                var hash = KeyToKnotHash(n);
                list.Add(hash);
            }
            return list;
        }
        static string KeyToKnotHash(string key) {
            var list = new List<int>(256);
            for (int i = 0; i < 256; i++)
                list.Add(i);
            return DenseHashToHex(DenseHash(SparseHash(list, StringToBytes(key))));
        }
        static List<int> StringToBytes(string s) {
            var list = new List<int>();

            foreach (var c in s)
                list.Add((int)c);

            // Suffix
            list.AddRange(new List<int> { 17, 31, 73, 47, 23 });

            return list;
        }
        static string IntToHex(int val) {
            Func<int, char> toDigit = (i) => {
                if (i < 10) return i.ToString()[0];
                switch (i) {
                    case 10: return 'a';
                    case 11: return 'b';
                    case 12: return 'c';
                    case 13: return 'd';
                    case 14: return 'e';
                    case 15: return 'f';
                }
                return '?';
            };
            return string.Format("{0}{1}", toDigit(val / 16), toDigit(val % 16));
        }
        static List<int> SparseHash(List<int> input, List<int> pattern) {
            var list = new List<int>(input);
            var skipSize = 0;
            var pos = 0;

            for (int x = 0; x < 64; x++) { 
                foreach (var length in pattern) {
                    for (var i = 0; i < length / 2; i++) {
                        var a = list[(pos + i) % list.Count];
                        list[(pos + i) % list.Count] = list[(pos + length - i - 1) % list.Count];
                        list[(pos + length - i - 1) % list.Count] = a;
                    }
                    pos = (pos + length + skipSize) % list.Count;
                    skipSize++;
                }
            }

            return list;
        }
        static List<int> DenseHash(List<int> list) {
            var ret = new List<int>(16);
            var block = list[0];

            for (var i = 1; i < list.Count; i++) {
                if (i % 16 == 0) {
                    ret.Add(block);
                    block = list[i];
                }
                else {
                    block = block ^ list[i];
                }
            }
            ret.Add(block);
            return ret;
        }
        static string DenseHashToHex(List<int> list) {
            var str = new StringBuilder();

            foreach (var i in list) {
                str.Append(IntToHex(i));
            }
            return str.ToString();
        }
    }
}
