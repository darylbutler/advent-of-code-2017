﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day16
{
    class Program
    {
        /* --- Day 16: Permutation Promenade ---
         * 
         * You come upon a very unusual sight; a group of programs here appear to be dancing.
         * 
         * There are sixteen programs in total, named a through p. They start by standing in a line: 
         * a stands in position 0, b stands in position 1, and so on until p, which stands in position 15.
         * 
         * The programs' dance consists of a sequence of dance moves:
         * 
         *     Spin, written sX, makes X programs move from the end to the front, but maintain their order otherwise. (For example, s3 on abcde produces cdeab).
         *     Exchange, written xA/B, makes the programs at positions A and B swap places.
         *     Partner, written pA/B, makes the programs named A and B swap places.
         * 
         * For example, with only five programs standing in a line (abcde), they could do the following dance:
         * 
         *     s1, a spin of size 1: eabcd.
         *     x3/4, swapping the last two programs: eabdc.
         *     pe/b, swapping programs e and b: baedc.
         * 
         * After finishing their dance, the programs end up in order baedc.
         * 
         * You watch the dance for a while and record their dance moves (your puzzle input). In what order are the programs standing after their dance?
         * 
         * --- Part Two ---
         * 
         * Now that you're starting to get a feel for the dance moves, you turn your attention to the dance as a whole.
         * 
         * Keeping the positions they ended up in from their previous dance, the programs perform it again and again: including the first dance, 
         * a total of one billion (1000000000) times.
         * 
         * In the example above, their second dance would begin with the order baedc, and use the same dance moves:
         * 
         *     s1, a spin of size 1: cbaed.
         *     x3/4, swapping the last two programs: cbade.
         *     pe/b, swapping programs e and b: ceadb.
         * 
         * In what order are the programs standing after their billion dances?
         * 
         * 
         */

        static string Dancers = string.Empty;

        static void Main(string[] args) {
            // Build Program
            Dancers = "abcdefghijklmnop";

            var test = "x6/12,s14";
            var input = System.IO.File.ReadAllText("input.txt");

            //Console.WriteLine(Dancers);
            //var times = 1;              // Part 1
            var times = 1000000000;   // Part 2            
            //for (int i = 0; i < times; i++) {
            //    PerformDance(input);
            //}            
            //Console.WriteLine(Dancers);

            var x = PerformDanceMoves(input, times);
            var state = string.Join("", x);
            Console.WriteLine("Out1: {0}", state);

            // Get start
            //x = PerformDanceMoves(input, 1);
            //var key = GenerateTranslateKey(x);
            //for (int i = 0; i < times - 1; i++) {
            //    x = ApplyKey(x, key);
            //}
            //Console.WriteLine("Out2: {0}", string.Join("", x));

            Console.ReadLine();
        }
        static void PerformDance(string moves) {
            foreach (var move in moves.Split(',')) {
                if (move.Length < 1) continue;

                if (move[0] == 's') { // x from END to FRONT
                    int howMany = int.Parse(move.Substring(1));


                    var leftOver = Dancers.Substring(Dancers.Length - howMany);
                    Dancers = leftOver + Dancers.Substring(0, Dancers.Length - howMany);

                    var dncrs = new char[16];
                    char temp = 'a';
                    for (int i = 0; i < Dancers.Length; i++) {

                    }


                }
                else if (move[0] == 'x') { // Swap by index
                    var split = move.Substring(1).Split('/');
                    var a = int.Parse(split[0]);
                    var b = int.Parse(split[1]);

                    var t = string.Empty;
                    for (int i = 0; i < Dancers.Length; i++) {
                        if (i == a)
                            t += Dancers[b];
                        else if (i == b)
                            t += Dancers[a];
                        else
                            t += Dancers[i];
                    }

                    Dancers = t;
                }
                else if (move[0] == 'p') {
                    var split = move.Substring(1).Split('/');
                    var a = char.Parse(split[0]);
                    var b = char.Parse(split[1]);

                    var t = string.Empty;
                    for (int i = 0; i < Dancers.Length; i++) {
                        if (Dancers[i] == a)
                            t += b;
                        else if (Dancers[i] == b)
                            t += a;
                        else
                            t += Dancers[i];
                    }

                    Dancers = t;
                }
            }
        }
        struct Move
        {
            public enum MoveType
            {
                EndToFront,
                Swap,
                Exchange,
            };
            public MoveType Type;
            public int P1, P2;
        }
        static List<Move> StringToMoves(string moves) {
            var moveList = new List<Move>();

            foreach (var move in moves.Split(',')) {
                if (move.Length < 1) continue;

                if (move[0] == 's') { // x from END to FRONT
                    moveList.Add(new Move() {
                        Type = Move.MoveType.EndToFront,
                        P1 = 16 - int.Parse(move.Substring(1)),
                        P2 = 0  // Not used
                    });
                }
                else if (move[0] == 'x') { // Swap by index
                    var split = move.Substring(1).Split('/');
                    moveList.Add(new Move() {
                        Type = Move.MoveType.Swap,
                        P1 = int.Parse(split[0]),
                        P2 = int.Parse(split[1])
                    });
                }
                else if (move[0] == 'p') {
                    var split = move.Substring(1).Split('/');
                    moveList.Add(new Move() {
                        Type = Move.MoveType.Exchange,
                        P1 = char.Parse(split[0]),
                        P2 = char.Parse(split[1])
                    });
                }
            }
            return moveList;
        }

        static char[] PerformDanceMoves(string moveStr, int times = 1) {
            var dancers = new char[16];
            var temp = new char[16];
            var replaced = 0;
            var start = 0;
            var history = new List<string> {
                "abcdefghijklmnop"
            };

            // -- Setup
            for (int i = 0; i < 16; i++) {
                dancers[i] = (char)('a' + i);
            }

            // -- Get move list
            var moves = StringToMoves(moveStr);

            // -- Repeat Moves on List  
            var timeK = 0L;
            var sw = System.Diagnostics.Stopwatch.StartNew();
            for (int time = 0; time < (times % 60); time++) {
                if (time > 100_000) {
                    timeK = sw.ElapsedMilliseconds;
                    sw.Restart();
                }

                foreach (var move in moves) {
                    switch (move.Type) {
                        case Move.MoveType.EndToFront:
                            start = (start + move.P1) % 16;
                            break;
                        case Move.MoveType.Swap:
                            temp[0] = dancers[(start + move.P1) % 16];
                            dancers[(start + move.P1) % 16] = dancers[(start + move.P2) % 16];
                            dancers[(start + move.P2) % 16] = temp[0];
                            break;
                        case Move.MoveType.Exchange:
                            replaced = 0;
                            for (int i = 0; i < 16; i++) {
                                if (dancers[i] == (char)move.P1) {
                                    dancers[i] = (char)move.P2;
                                    replaced++;
                                }
                                else if (dancers[i] == (char)move.P2) {
                                    dancers[i] = (char)move.P1;
                                    replaced++;
                                }

                                if (replaced > 1) break;
                            }
                            break;
                    }
                }

                // debug
                for (int i = 0; i < 16; i++) {
                    temp[i] = dancers[(start + i) % 16];
                }
                //var state = string.Join("", temp);
                //if (history.Contains(state))
                //    Console.WriteLine("State Repeated: {0}, Iter: {1}", state, time);
                //history.Add(state);
            }

            for (int i = 0; i < 16; i++) {
                temp[i] = dancers[(start + i) % 16];
            }
            return temp;
        }

        static int[] GenerateTranslateKey(char[] end) {                        
            var key = new int[16];

            for (int i = 0; i < 16; i++) {
                key[i] = end[i] - 'a';
            }
            return key;
        }
        static char[] ApplyKey(char[] start, int[] key) {
            var end = new char[16];

            for (int i = 0; i < 16; i++) {
                end[i] = start[key[i]];
            }
            return end;
        }
    }
}
