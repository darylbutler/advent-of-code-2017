﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day4
{

    /**
     * Part 1-
     * A new system policy has been put in place that requires all accounts to use a passphrase instead of simply a password. A passphrase consists of a series of words (lowercase letters) separated by spaces.
     * 
     * To ensure security, a valid passphrase must contain no duplicate words.
     * 
     * For example:
     * 
     *     aa bb cc dd ee is valid.
     *     aa bb cc dd aa is not valid - the word aa appears more than once.
     *     aa bb cc dd aaa is valid - aa and aaa count as different words.
     * 
     * The system's full passphrase list is available as your puzzle input. How many passphrases are valid?
     * 
     * Part 2-
     * 
     * 
     */
    class Program
    {
        static void Main(string[] args) {
            var input = File.ReadAllLines("input.txt");

            // Part 1 - Count of valid phrases
            Console.WriteLine("Part 1:");
            var valid = 0;
            foreach (var phrase in input) {
                if (ValidPassphrase_Part1(phrase))
                    valid++;
            }
            Console.WriteLine("Valid Phrases: {0}", valid);

            // Part 2 - 
            Console.WriteLine("Part 2:");
            //Console.WriteLine("abcde fghij: {0}", ValidPassphrase_Part2("abcde fghij"));
            //Console.WriteLine("abcde xyz ecdab: {0}", ValidPassphrase_Part2("abcde xyz ecdab"));
            //Console.WriteLine("a ab abc abd abf abj: {0}", ValidPassphrase_Part2("a ab abc abd abf abj"));
            //Console.WriteLine("iiii oiii ooii oooi oooo: {0}", ValidPassphrase_Part2("iiii oiii ooii oooi oooo"));
            //Console.WriteLine("oiii ioii iioi iiio: {0}", ValidPassphrase_Part2("oiii ioii iioi iiio"));

            valid = 0;
            foreach (var phrase in input) {
                if (ValidPassphrase_Part2(phrase))
                    valid++;
            }
            Console.WriteLine("Valid Phrases: {0}", valid);

            Console.ReadLine();
        }

        static bool ValidPassphrase_Part1(string phrase) {
            var split = phrase.Split(' ');
            var words = new List<string>();

            foreach (var word in split) {
                if (words.Contains(word))
                    return false;
                words.Add(word);
            }
            return true;
        }

        static bool ValidPassphrase_Part2(string phrase) {
            var split = phrase.Split(' ');
            var words = new List<string>();

            foreach (var word in split) {
                foreach (var w in words) {
                    if (WordsAreAnagrams(w, word))
                        return false;
                }

                words.Add(word);
            }
            return true;
        }

        static bool WordsAreAnagrams(string word1, string word2) {
            var letters = word1.ToCharArray().ToList();
            foreach (var c in word2) {
                if (!letters.Contains(c))
                    return false;   // All letters must be used
                letters.Remove(c);
            }
            return letters.Count < 1;   // If no letters left, then these words are anagrams
        }
    }
}
