﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day18
{
    class Program
    {
        /* --- Day 18: Duet ---
         * 
         * You discover a tablet containing some strange assembly code labeled simply "Duet". Rather than bother the sound card with it, you decide to run the code yourself. Unfortunately, you don't see any documentation, so you're left to figure out what the instructions mean on your own.
         * 
         * It seems like the assembly is meant to operate on a set of registers that are each named with a single letter and that can each hold a single integer. You suppose each register should start with a value of 0.
         * 
         * There aren't that many instructions, so it shouldn't be hard to figure out what they do. Here's what you determine:
         * 
         *     snd X plays a sound with a frequency equal to the value of X.
         *     set X Y sets register X to the value of Y.
         *     add X Y increases register X by the value of Y.
         *     mul X Y sets register X to the result of multiplying the value contained in register X by the value of Y.
         *     mod X Y sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
         *     rcv X recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
         *     jgz X Y jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
         * 
         * Many of the instructions can take either a register (a single letter) or a number. The value of a register is the integer it contains; the value of a number is that number.
         * 
         * After each jump instruction, the program continues with the instruction to which the jump jumped. After any other instruction, the program continues with the next instruction. Continuing (or jumping) off either end of the program terminates it.
         * 
         * For example:
         * 
         * set a 1
         * add a 2
         * mul a a
         * mod a 5
         * snd a
         * set a 0
         * rcv a
         * jgz a -1
         * set a 1
         * jgz a -2
         * 
         *     The first four instructions set a to 1, add 2 to it, square it, and then set it to itself modulo 5, resulting in a value of 4.
         *     Then, a sound with frequency 4 (the value of a) is played.
         *     After that, a is set to 0, causing the subsequent rcv and jgz instructions to both be skipped (rcv because a is 0, and jgz because a is not greater than 0).
         *     Finally, a is set to 1, causing the next jgz instruction to activate, jumping back two instructions to another jump, which jumps again to the rcv, which ultimately triggers the recover operation.
         * 
         * At the time the recover operation is executed, the frequency of the last sound played is 4.
         * 
         * What is the value of the recovered frequency (the value of the most recently played sound) the first time a rcv instruction is executed with a non-zero value?
         * 
         * 
         */
        static void Main(string[] args) {
            var test = new List<string> {
                "set a 1",
                "add a 2",
                "mul a a",
                "mod a 5",
                "snd a",
                "set a 0",
                "rcv a",
                "jgz a -1",
                "set a 1",
                "jgz a -2",
            };
            var part2_tst = new List<string> {
                "snd 1",
                "snd 2",
                "snd p",
                "rcv a",
                "rcv b",
                "rcv c",
                "rcv d",
            };
            var input = new List<string> {
                "set i 31",
                "set a 1",
                "mul p 17",
                "jgz p p",
                "mul a 2",
                "add i -1",
                "jgz i -2",
                "add a -1",
                "set i 127",
                "set p 952",
                "mul p 8505",
                "mod p a",
                "mul p 129749",
                "add p 12345",
                "mod p a",
                "set b p",
                "mod b 10000",
                "snd b",
                "add i -1",
                "jgz i -9",
                "jgz a 3",
                "rcv b",
                "jgz b -1",
                "set f 0",
                "set i 126",
                "rcv a",
                "rcv b",
                "set p a",
                "mul p -1",
                "add p b",
                "jgz p 4",
                "snd a",
                "set a b",
                "jgz 1 3",
                "snd b",
                "set f 1",
                "add i -1",
                "jgz i -11",
                "snd a",
                "jgz f -16",
                "jgz a -19",
            };


            //Run(input);
            var t = Run_p2(input);
            Console.WriteLine(t);

            Console.ReadLine();
        }
       

        public static void Run(List<string> inst) {
            Console.WriteLine("Beginning Execution of program with {0} instructions...", inst.Count);
            var steps = new List<string>();            
            var lastPlayedFreq = 0L;
            var lastRecoveredPlayedSoundFreq = 0L;
            var registers = new long[27];

            // -- If string of int, return int. Else, return associated register
            Func<string, long> getValue = (code) => {
                if (int.TryParse(code, out int val)) return val;
                if (code.Length == 1) {
                    var reg = code[0] - 'a';
                    return registers[reg];
                }
                throw new Exception("Unknown value: " + code);
            };
            // -- Convert string char to number, and return value from local register array
            Func<string, long> getRegister = (reg) => {
                if (reg.Length == 1) {
                    return registers[reg[0] - 'a'];
                }
                throw new Exception("Unknown register: " + reg);
            };
            // -- Convert string char to number, then set that register to value
            Action<string, long> setRegister = (reg, value) => {
                if (reg.Length == 1) {
                    registers[reg[0] - 'a'] = value;
                }
                else {
                    throw new Exception("Unknown register: " + reg);
                }
            };

            // Process Each Code
            for (int i = 0; i < inst.Count; i++) {
                var parts = inst[i].Split(' ');

                if (parts.Length != 2 && parts.Length != 3) throw new Exception("Bad Instruction: " + inst[i]);
                steps.Add(inst[i]);

                // "snd X" plays a sound with a frequency equal to the value of X
                if (parts[0] == "snd") {                    
                    lastPlayedFreq = getValue(parts[1]);
                    Console.WriteLine("Sound Played @ Freq {0}Hz", lastPlayedFreq);
                }
                // "set X Y" sets register X to the value of Y.
                else if (parts[0] == "set") {
                    setRegister(parts[1], getValue(parts[2]));
                }
                // "add X Y" increases register X by the value of Y.
                else if (parts[0] == "add") {
                    setRegister(parts[1], getRegister(parts[1]) + getValue(parts[2]));
                }
                // "mul X Y" sets register X to the result of multiplying the value contained in register X by the value of Y.
                else if (parts[0] == "mul") {
                    setRegister(parts[1], getRegister(parts[1]) * getValue(parts[2]));
                }
                // "mod X Y" sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
                else if (parts[0] == "mod") {
                    setRegister(parts[1], getRegister(parts[1]) % getValue(parts[2]));
                }
                // "rcv X" recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
                else if (parts[0] == "rcv") {
                    if (getValue(parts[1]) != 0) {                        
                        lastRecoveredPlayedSoundFreq = lastPlayedFreq;
                        Console.WriteLine("Recovered Played Frequency {0}Hz", lastRecoveredPlayedSoundFreq);
                        return;
                    }
                }
                // "jgz X Y" jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
                else if (parts[0] == "jgz") {
                    if (getValue(parts[1]) > 0) {
                        i += (int)(getValue(parts[2]) - 1);
                        
                        // Check for out of range, then exit
                        if (i < 0 || i >= inst.Count) {
                            Console.WriteLine("Instructions Completed. Exiting.");
                            return;
                        }
                    }
                }
                // Bad Instruction
                else {                    
                    throw new Exception("Unknown Instruction: " + inst[i]);
                }                
            }
        }

        
        public static long Run_p2(List<string> inst) {            
            // -- Registers
            var registers = new long[2,27];
            registers[0, 'p' - 'a'] = 0;
            registers[1, 'p' - 'a'] = 1;

            // -- If string of int, return int. Else, return associated register
            Func<string, int, long> getValue = (code, prog) => {
                if (int.TryParse(code, out int val)) return val;
                if (code.Length == 1) {
                    var reg = code[0] - 'a';
                    return registers[prog, reg];
                }
                throw new Exception("Unknown value: " + code);
            };
            // -- Convert string char to number, and return value from local register array
            Func<string, int, long> getRegister = (reg, prog) => {
                if (reg.Length == 1) {
                    return registers[prog, reg[0] - 'a'];
                }
                throw new Exception("Unknown register: " + reg);
            };
            // -- Convert string char to number, then set that register to value
            Action<string, int, long> setRegister = (reg, prog, value) => {
                if (reg.Length == 1) {
                    registers[prog, reg[0] - 'a'] = value;
                }
                else {
                    throw new Exception("Unknown register: " + reg);
                }
            };

            // Status vars            
            var Queue = new Queue<long>[2];     // prg <> prg queue (prg0 SND to queue[1] and RCV from queue[0] )
                Queue[0] = new Queue<long>();
                Queue[1] = new Queue<long>();
            var WaitingForRCV = new bool[2];    // true if prg is waiting for input
            var ProgramFinished = new bool[2];  // true if prg is done executing
            var Step = new int[2];              // index of inst this program is executing

            // Return var
            var PrgSent = new int[2];           // number of times each prg sends to the other

            while(true) {
                // -- Exit condition: Both threads deadlocked
                if (WaitingForRCV[0] && WaitingForRCV[1] && Queue[0].Count < 1 && Queue[1].Count < 1)
                    return PrgSent[1];
                // -- Exit condition: Both threads are done
                if (ProgramFinished[0] && ProgramFinished[1])
                    return PrgSent[1];

                // -- Run each program...
                for (int prg = 0; prg < 2; prg++) {
                    if (ProgramFinished[prg]) continue;

                    // -- Execute current step
                    var parts = inst[Step[prg]].Split(' ');

                    // -- Check if we can recieve input
                    if (WaitingForRCV[prg] && Queue[prg].Count > 0) {
                        setRegister(parts[1], prg, Queue[prg].Dequeue());
                        WaitingForRCV[prg] = false;
                        Step[prg]++;
                    }
                    else if (!WaitingForRCV[prg]) {
                        // -- Perform instruction
                        
                        // "snd X" sends the value of X to the other program. These values wait in a queue until that program is ready to receive them. Each program has its own message queue, so a program can never receive a message it sent.
                        if (parts[0] == "snd") {
                            Queue[prg == 0 ? 1 : 0].Enqueue(getValue(parts[1], prg));
                            PrgSent[prg]++;
                            Step[prg]++;
                        }
                        // "set X Y" sets register X to the value of Y.
                        else if (parts[0] == "set") {
                            setRegister(parts[1], prg, getValue(parts[2], prg));
                            Step[prg]++;
                        }
                        // "add X Y" increases register X by the value of Y.
                        else if (parts[0] == "add") {
                            setRegister(parts[1], prg, getRegister(parts[1], prg) + getValue(parts[2], prg));
                            Step[prg]++;
                        }
                        // "mul X Y" sets register X to the result of multiplying the value contained in register X by the value of Y.
                        else if (parts[0] == "mul") {
                            setRegister(parts[1], prg, getRegister(parts[1], prg) * getValue(parts[2], prg));
                            Step[prg]++;
                        }
                        // "mod X Y" sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
                        else if (parts[0] == "mod") {
                            setRegister(parts[1], prg, getRegister(parts[1], prg) % getValue(parts[2], prg));
                            Step[prg]++;
                        }
                        // "rcv X" receives the next value and stores it in register X. If no values are in the queue, the program waits for a value to be sent to it. Programs do not continue to the next instruction until they have received a value. Values are received in the order they are sent.
                        else if (parts[0] == "rcv") {
                            WaitingForRCV[prg] = true;
                        }
                        // "jgz X Y" jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
                        else if (parts[0] == "jgz") {
                            if (getValue(parts[1], prg) > 0) {
                                Step[prg] += (int)getValue(parts[2], prg);

                                // Check for out of range, then exit
                                if (Step[prg] < 0 || Step[prg] >= inst.Count) {
                                    ProgramFinished[prg] = true;
                                }
                            } else {
                                Step[prg]++;
                            }
                        }
                        // Bad Instruction
                        else {
                            throw new Exception("Unknown Instruction: " + string.Join(" ", parts));
                        }
                    }
                }
            }
        }
    }
}
