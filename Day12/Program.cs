﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day12
{
    class Program
    {
        /* --- Day 12: Digital Plumber ---
         * 
         * Walking along the memory banks of the stream, you find a small village that is experiencing a little confusion: some programs can't communicate with each other.
         * 
         * Programs in this village communicate using a fixed system of pipes. Messages are passed between programs using these pipes, but most programs aren't connected to each other directly. Instead, programs pass messages between each other until the message reaches the intended recipient.
         * 
         * For some reason, though, some of these messages aren't ever reaching their intended recipient, and the programs suspect that some pipes are missing. They would like you to investigate.
         * 
         * You walk through the village and record the ID of each program and the IDs with which it can communicate directly (your puzzle input). Each program has one or more programs with which it can communicate, and these pipes are bidirectional; if 8 says it can communicate with 11, then 11 will say it can communicate with 8.
         * 
         * You need to figure out how many programs are in the group that contains program ID 0.
         * 
         * For example, suppose you go door-to-door like a travelling salesman and record the following list:
         * 
         * 0 <-> 2
         * 1 <-> 1
         * 2 <-> 0, 3, 4
         * 3 <-> 2, 4
         * 4 <-> 2, 3, 6
         * 5 <-> 6
         * 6 <-> 4, 5
         * 
         * In this example, the following programs are in the group that contains program ID 0:
         * 
         *     Program 0 by definition.
         *     Program 2, directly connected to program 0.
         *     Program 3 via program 2.
         *     Program 4 via program 2.
         *     Program 5 via programs 6, then 4, then 2.
         *     Program 6 via programs 4, then 2.
         * 
         * Therefore, a total of 6 programs are in this group; all but program 1, which has a pipe that connects it to itself.
         * 
         * How many programs are in the group that contains program ID 0?
         * 
         * --- Part Two ---
         * 
         * There are more programs than just the ones in the group containing program ID 0. The rest of them have no way of reaching that group, and still might have no way of reaching each other.
         * 
         * A group is a collection of programs that can all communicate via pipes either directly or indirectly. The programs you identified just a moment ago are all part of the same group. Now, they would like you to determine the total number of groups.
         * 
         * In the example above, there were 2 groups: one consisting of programs 0,2,3,4,5,6, and the other consisting solely of program 1.
         * 
         * How many groups are there in total?
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         */
        static Dictionary<int, List<int>> Programs = new Dictionary<int, List<int>>();
        static void Main(string[] args) {
            //AddPrgFromString("0 <-> 2");
            //AddPrgFromString("1 <-> 1");
            //AddPrgFromString("2 <-> 0, 3, 4");
            //AddPrgFromString("3 <-> 2, 4");
            //AddPrgFromString("4 <-> 2, 3, 6");
            //AddPrgFromString("5 <-> 6");
            //AddPrgFromString("6 <-> 4, 5");
            //AddPrgFromString("7 <-> 9");
            //AddPrgFromString("8 <-> 1");
            //AddPrgFromString("9 <-> 8");
            //Console.WriteLine("1 is connected to 0: {0}", Connected(1, 0));
            //Console.WriteLine("2 is connected to 0: {0}", Connected(2, 0));
            //Console.WriteLine("3 is connected to 0: {0}", Connected(3, 0));
            //Console.WriteLine("4 is connected to 0: {0}", Connected(4, 0));
            //Console.WriteLine("5 is connected to 0: {0}", Connected(5, 0));
            //Console.WriteLine("6 is connected to 0: {0}", Connected(6, 0));
            foreach (var line in System.IO.File.ReadAllLines("input.txt")) AddPrgFromString(line);

            Console.WriteLine("--- Part 1:");
            Console.WriteLine("Count: {0}", Programs.Keys.Where(x => Connected(x, 0)).Select(x => x).Count());

            Console.WriteLine("--- Part 2:");
            var groups = new Dictionary<int, List<int>>();    // <prg> <prgs in group>
            
            foreach (var prg in Programs.Keys) {
                // See if this is connected to any programs in the group already
                var added = false;
                foreach (var grp in groups.Keys) {
                    if (Connected(prg, grp)) {
                        groups[grp].Add(prg);
                        added = true;
                        break;
                    }
                }
                if (added) continue;

                // if not, add to the groups
                groups[prg] = new List<int> { prg };
            }
            Console.WriteLine("Groups: {0}", groups.Keys.Count);

            Console.ReadLine();
        }
        static void AddPrgFromString(string s) {
            var id = -1;
            var pipes = new List<int>();
            var start = 0;
            for (var i = 1; i < s.Length; i++) {
                if (start == 0 && s[i] == '<') {
                    id = int.Parse(s.Substring(0, i));
                }
                else if (s[i] == '>') {
                    start = i + 1;
                }
                else if (s[i] == ',' || i == s.Length - 1) {
                    var sub = "";
                    if (i == s.Length - 1)
                        sub = s.Substring(start);
                    else
                        sub = s.Substring(start, i - start);

                    pipes.Add(int.Parse(sub));
                    start = i + 1;
                }
            }

            Programs[id] = pipes;
        }
        static bool Connected(int id, int dest, string history = "") {
            if (!Programs.ContainsKey(id) || Programs[id].Count < 1)
                return false;

            foreach (var pipe in Programs[id]) {
                if (pipe == dest)
                    return true;
                if (pipe == id || history.Contains(string.Format(" {0} ", pipe)))
                    continue;

                if (Connected(pipe, dest, string.Format("{0} {1} ", history, id)))
                    return true;                                  
            }
            return false;
        }
        
    }
}
